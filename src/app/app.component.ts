import { Component } from '@angular/core';
import { interval } from 'rxjs/observable/interval';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
styleUrls: ['./app.component.css']
})
export class AppComponent {
  countdown: any = '60';
  interval: any;
  isHidden = true;
  stats = 'Statystyki:';

  title = 'Tabliczka Mnożenia. Próba czasu';
  Start() {
    console.log('jest start');
    this.contdown(60);
    this.isHidden = false;
    console.log(this.countdown);
    }
    Reset() {
      clearInterval(this.interval);
      this.countdown = '60' ;
      console.log('jest reset');
      this.isHidden = true;
      this.stats = 'Statystyki:';

    }

    contdown(countdown) {
        if ( this.countdown > 0 ) {
        this.interval = setInterval(
          () => {
              countdown--;
              console.log(countdown);
              if ( this.countdown === 1 ) { clearInterval(this.interval);
              this.isHidden = true;
            this.stats = 'Twój Wynik:';
          }

            return this.countdown = countdown + 1;
          }, 1000 );
     }
    }

}
