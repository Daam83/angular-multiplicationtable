import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CalculationsComponent } from './calculations.component';
import { AppComponent } from '../app.component';
import { FormsModule } from '@angular/forms';
import { Values } from './values';
import { DebugElement } from '@angular/core/src/debug/debug_node';
import { By } from '@angular/platform-browser/src/dom/debug/by';

describe('CalculationsComponent', () => {
  let component: CalculationsComponent;
  let fixture: ComponentFixture<CalculationsComponent>;
  let input: DebugElement;
  let inputElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ AppComponent,
        CalculationsComponent
         ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it(`should be '0 / 0 / 0' `, () => {
expect(component.good).toBe(0);
expect(component.bad).toBe(0);
expect(component.all).toBe(0);
  });
  it(`should be ' Wait for answer'`, () => {
  expect(component.res).toBe(' Wait for answer');
  });
  it(`should be '1 / 0 / 1' and comments will be 'Correct !!'`, () => {
    component.answer = component.values.number2;
    component.correctOrNot();
    expect(component.good).toBe(1);
    expect(component.bad).toBe(0);
    expect(component.all).toBe(1);
    expect(component.res).toBe('Correct !!');
  });
  it(`should be '0 / 1 / 1' and comments will be 'Incorrect !! '`, () => {
    component.answer = component.values.number2 + 1;
    component.correctOrNot();
    expect(component.good).toBe(0);
    expect(component.bad).toBe(1);
    expect(component.all).toBe(1);
    expect(component.res).toBe('Incorrect !! ');
  });
  it('number are bind by 1. should be 1 / 0 / 1', () => {
    component.values.number1 = 1;
    component.values.number2 = 1;
    component.answer = 1;
    component.correctOrNot();
    expect(component.good).toBe(1);
    expect(component.bad).toBe(0);
    expect(component.all).toBe(1);
    expect(component.res).toBe('Correct !!');

  });
});
