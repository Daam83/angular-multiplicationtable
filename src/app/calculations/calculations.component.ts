import { Component, OnInit, Input } from '@angular/core';
import {Values} from './values';
import { isString } from 'util';
import { STRING_TYPE } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-calculations',
  templateUrl: './calculations.component.html',
  styleUrls: ['./calculations.component.css']
})
export class CalculationsComponent implements OnInit {
   values: Values = {
    number1: this.randomInt(1, 10),
    number2: this.randomInt(1, 10),
    result: 0,
     };
  result = this.values.number1 * this.values.number2;

  res = ' Wait for answer';
  answer: number;
  all: number;
  good: number;
  bad: number;
  @Input()    isHidden: boolean;

  constructor() { }

  ngOnInit() {
    this.all = 0;
    this.good = 0;
    this.bad = 0;

  }
/*
  * generate a random integer between min and max
  * @param {Number} min
  * @param {Number} max
  * @return {Number} random generated integer
  * download from https://gist.github.com/ValeryToda/fbf1de017f91c0ec3da04116c5ccf8b5
  */
  randomInt(min, max) {
     return Math.floor(Math.random() * (max - min + 1)) + min;
 }
  correctOrNot() {
    ++this.all;
    console.log(this.values.number1 + ' ' + this.result);

    if ( this.answer === this.values.number2) {
    this.res = 'Correct !!';
    console.log(this.values.number1 + ' ' + this.result);
    ++this.good;
    this.generateCalculation();

    } else {
      console.log(this.values.number1 + ' ' + this.result);
    this.res = 'Incorrect !! ';
    ++this.bad;
    this.generateCalculation();

    }

  }

  generateCalculation() {
    this.result = 0;
    this.values.number1 = this.randomInt(1, 10);
    this.values.number2 = this.randomInt(1, 10);
    this.result = this.values.number1 * this.values.number2;
    console.log('z generatora ' + this.values.number1 + ' ' + this.values.number2 + ' ' + this.result);

  }
}
